export const loadMessages = (data) => {
  return {
    type: "LOAD_MESSAGES",
    data,
  };
};

export const turnOffLoad = () => {
  return {
    type: "IS_PRELOADER",
  };
};

export const updateOnlineDate = (data) => {
  return {
    type: "UPDATE_ONLINE_DATE",
    data,
  };
};

export const updateUsersInChat = (data) => {
  return {
    type: "UPDATE_USERS_IN_CHAT",
    data,
  };
};

export const updateMessagesCountInChat = (data) => {
  return {
    type: "UPDATE_MESSAGES_IN_CHAT",
    data,
  };
};

export const editMessage = (id, text) => {
  return {
    type: "UPDATE_MESSAGE",
    id: id,
    text: text,
  };
};

export const showModalWindow = (logicData) => {
  return {
    type: "MODAL_WINDOW_EDIT",
    logicData,
  };
};

export const tempText = (text) => {
  return {
    type: "SAVE_MESSAGE",
    text,
  };
};

export const tempId = (id) => {
  return {
    type: "UPDATE_MESSAGE_ID",
    id,
  };
};
