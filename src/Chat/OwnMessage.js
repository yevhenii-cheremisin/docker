import React from "react";
import "./OwnMessage.css";

import { connect } from "react-redux";
import { showModalWindow, tempText, tempId } from "../actions/";

class OwnMessage extends React.Component {
  constructor() {
    super();
    this.editMes = this.editMes.bind(this);
    this.deleteMes = this.deleteMes.bind(this);
    this.showModal = this.showModal.bind(this);
  }
  renderTime(createdAt) {
    const date = new Date(createdAt);
    return (
      date.getHours().toString().padStart(2, "0") +
      ":" +
      date.getMinutes().toString().padStart(2, "0")
    );
  }

  editMes() {
    this.props.tempId(this.props.data.id);
    this.props.tempText(this.props.data.text);
  }

  showModal() {
    this.props.showModalWindow(true);
    this.editMes();
  }

  deleteMes() {
    const id = this.props.data.id;
    this.props.delete(id);
  }

  render() {
    let date = this.renderTime(this.props.data.createdAt);

    return (
      <div className="own-message">
        <button className="message-edit" onClick={this.showModal}>
          🖊️
        </button>
        <button className="message-delete" onClick={this.deleteMes}>
          🗑️
        </button>
        <div className="message-body">
          <div className="message-body_text-own">
            <span className="message-text">{this.props.data.text}</span>
          </div>
          <span className="message-time">{date}</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { reducer: state.chat };
};

const mapDispatchToProps = () => {
  return {
    showModalWindow,
    tempText,
    tempId,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(OwnMessage);
