import React from "react";
import "./Preloader.css";

class Preloader extends React.Component {
  render() {
    return (
      <div className="preloader">
        <div className="lds-ripple">
          <div></div>
          <div></div>
        </div>
      </div>
    );
  }
}

export default Preloader;
