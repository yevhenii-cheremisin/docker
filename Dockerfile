FROM node:15.13-alpine

WORKDIR /docker

COPY . .

RUN npm install
RUN npm run build

CMD ["npm", "start"]